package es.um.es.semanticweb.translator.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDocumentFormat;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.search.EntitySearcher;


public class Utils {
	/**
	 * Return true if a file exists in path.
	 * @param path The path of the file.
	 * @return True if a file exists in path, false if not.
	 */
	public static boolean fileExists(String path){
		File file = new File(path);
		return file.exists() && file.isFile();
	}
	
	
	
	public static OWLOntology loadOntologyFromPath(String ontPath) throws OWLOntologyCreationException, FileNotFoundException {
		OWLOntology ontology = null;
		InputStream streamSource = new FileInputStream(new File(ontPath));
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		ontology = manager.loadOntologyFromOntologyDocument(streamSource);
		return ontology;
	}
	
	public static void saveOntology (OWLOntology ontModel, String path) throws OWLOntologyStorageException, FileNotFoundException{
		OWLDocumentFormat format = ontModel.getOWLOntologyManager().getOntologyFormat(ontModel);
		ontModel.saveOntology(format, new FileOutputStream(path));
	}
	
	public static List<String> getLabels(OWLOntology ontology, OWLEntity owlEntity, String lang) {
		List<String> labels = new ArrayList<String>();

		if (owlEntity != null) {
			OWLDataFactory dataFactory = ontology.getOWLOntologyManager().getOWLDataFactory();
			Iterator<OWLAnnotation> annotationIterator = EntitySearcher
					.getAnnotations(owlEntity, ontology, dataFactory.getRDFSLabel()).iterator();
			
			while (annotationIterator.hasNext()) {
				OWLAnnotation annotation = annotationIterator.next();
				if (annotation.getValue() instanceof OWLLiteral) {
					OWLLiteral literal = (OWLLiteral) annotation.getValue();
					if (lang != null) {
						if (literal.hasLang(lang)) {
							labels.add(literal.getLiteral());
						}
					} else {
						labels.add(literal.getLiteral());
					}
				}
			}
		}
		return labels;
	}
	
	public static void addLabel(OWLOntology ontModel, OWLEntity entity, String label, String lang){
		OWLDataFactory df = ontModel.getOWLOntologyManager().getOWLDataFactory();
		OWLAnnotation annotation = df.getOWLAnnotation(df.getRDFSLabel(), df.getOWLLiteral(label, lang));
		OWLAxiom axiom = df.getOWLAnnotationAssertionAxiom(entity.getIRI(), annotation);
		ontModel.getOWLOntologyManager().applyChange(new AddAxiom(ontModel, axiom));
	}
}
