package es.um.es.semanticweb.translator.javaObjects;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;

/**
 * Class that translates text.
 * 
 * @author fabad
 *
 */
public class TextTranslator {
	private static final String SOURCE_LANG_PARAM = "${source_lang}";
	private static final String DEST_LANG_PARAM = "${dest_lang}";
	private static final String INPUT_PARAM = "${input}";
	private static final String REST_TRANSLATOR_URL = "https://translate.googleapis.com/translate_a/single?client=gtx&sl="
			+ SOURCE_LANG_PARAM + "&tl=" + DEST_LANG_PARAM + "&dt=t&q=" + INPUT_PARAM;

	/**
	 * Translates the text stored in "input" variable from sourceLang language
	 * to destLang language. sourceLang could be null or empty, in which case
	 * will be detected automatically. If destLang is null or empty, input will
	 * be translated into english by default.
	 * 
	 * @param sourceLang Source language. If null or empty, it will be recognized automatically.
	 * @param destLang Destination language. If null or empty, it will be English by default.
	 * @param input The input string to be translated.
	 * @return A String with the translation.
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public String translate(String sourceLang, String destLang, String input)
			throws ClientProtocolException, IOException {
		String translation = "";
		if (sourceLang == null || sourceLang.isEmpty()) {
			sourceLang = "auto";
		}

		if (destLang == null || destLang.isEmpty()) {
			destLang = "en";
		}
		if (input != null) {
			String urlGestRequest = this.createUrlGetRequest(sourceLang, destLang, input);

			HttpClient client = new DefaultHttpClient();
			HttpGet request = new HttpGet(urlGestRequest);
			HttpResponse response = client.execute(request);
			if (response.getStatusLine().getStatusCode() == 200) {

				BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				String line = "";
				StringBuilder responseString = new StringBuilder();
				while ((line = rd.readLine()) != null) {
					responseString.append(line);
				}
				translation = this.parseResult(responseString.toString());
			}
		}
		return translation;
	}

	private String createUrlGetRequest(String sourceLang, String destLang, String input)
			throws UnsupportedEncodingException {
		String urlRest = null;
		if (sourceLang != null && destLang != null && input != null) {
			urlRest = REST_TRANSLATOR_URL;
			urlRest = urlRest.replace(SOURCE_LANG_PARAM, sourceLang);
			urlRest = urlRest.replace(DEST_LANG_PARAM, destLang);
			urlRest = urlRest.replace(INPUT_PARAM, URLEncoder.encode(input, "UTF-8"));
		}
		return urlRest;
	}

	private String parseResult(String inputJson) {
		JSONArray jsonArray = new JSONArray(inputJson);
		JSONArray jsonArray2 = (JSONArray) jsonArray.get(0);
		JSONArray jsonArray3 = (JSONArray) jsonArray2.get(0);

		return jsonArray3.get(0).toString();
	}
}
