package es.um.es.semanticweb.translator.main;

import java.io.FileNotFoundException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;

import es.um.es.semanticweb.translator.javaObjects.OntologyTranslator;

public class MainClass {
	private static final String ONTOLOGY_PATH_OPTION = "f";
	private static final String DEST_ONTOLOGY_PATH_OPTION = "o";
	private static final String SOURCE_LANG_OPTION = "l";
	private static final String DEST_LANG_OPTION = "L";
	

	/**
	 * String ontologyPath, boolean overrideOntology, String sourceLang, String
	 * destLang, String destOntologyPath
	 */
	public static void main(String[] args) {
		CommandLine cmd;
		Option ontologyPathOption = Option.builder(ONTOLOGY_PATH_OPTION)
				.hasArg(true)
	            .required(true)
	            .desc("The path of the original ontology.")
	            .longOpt("ontologyPath")
	            .build();
		
		Option destOntologyPathOption = Option.builder(DEST_ONTOLOGY_PATH_OPTION)
				.hasArg(true)
	            .required(false)
	            .desc("The path to store the translated ontology. If nos specified, the original ontology will be overridden.")
	            .longOpt("destOntologyPath")
	            .build();
		
		Option sourceLangOption = Option.builder(SOURCE_LANG_OPTION)
	            .required(true)
	            .hasArg(true)
	            .desc("The language used to obtain the labels from the original ontology in order to translate them")
	            .longOpt("sourceLang")
	            .build();
		
		Option destLangOption = Option.builder(DEST_LANG_OPTION)
				.hasArg(true)
	            .required(true)
	            .desc("The destination lang to translate the labels from the ontology.")
	            .longOpt("destLang")
	            .build();
		

		
		Options options = new Options();
		options.addOption(ontologyPathOption);
		options.addOption(destOntologyPathOption);
		options.addOption(sourceLangOption);
		options.addOption(destLangOption);
		
		CommandLineParser parser = new DefaultParser();
		
		try {
			cmd = parser.parse(options, args);
			
			String ontologyPath = cmd.getOptionValue(ONTOLOGY_PATH_OPTION);
			String destOntologyPath = null;
			String sourceLang = cmd.getOptionValue(SOURCE_LANG_OPTION);
			String destLang = cmd.getOptionValue(DEST_LANG_OPTION);
			
			boolean overrideOntology = true;
			if(cmd.hasOption(DEST_ONTOLOGY_PATH_OPTION)){
				overrideOntology = false;
				destOntologyPath = cmd.getOptionValue(DEST_ONTOLOGY_PATH_OPTION);
			}
			OntologyTranslator ontologyTranslator = new OntologyTranslator(ontologyPath, overrideOntology, sourceLang, destLang, destOntologyPath);
			ontologyTranslator.translate();
			
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (OWLOntologyStorageException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
}
