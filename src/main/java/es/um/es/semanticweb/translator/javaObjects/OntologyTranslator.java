package es.um.es.semanticweb.translator.javaObjects;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.client.ClientProtocolException;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.search.EntitySearcher;

import es.um.es.semanticweb.translator.utils.Utils;

/**
 * Class that translates the labels of the ontology.
 * @author fabad
 *
 */
public class OntologyTranslator {
	
	private final static Logger LOGGER = Logger.getLogger(OntologyTranslator.class.getName());
	
	/**
	 * The ontology whose labels are going to be translated.
	 */
	private OWLOntology ontology;
	
	/**
	 * True if the translated ontology must be save overriding the original ontology.
	 */
	private boolean overrideOntology;
	
	/**
	 * The language from which retrieve the labels in order to translate them.
	 */
	private String sourceLang;
	
	/**
	 * The language to translate the labels.
	 */
	private String destLang;
	
	/**
	 * The path to store the translated ontology.
	 */
	private String destOntologyPath;
	
	/**
	 * The path from the original ontology has been read.
	 */
	private String origOntologyPath;
	
	/**
	 * The text translator
	 */
	private TextTranslator textTranslator;

	

	private OntologyTranslator(OWLOntology ontology, boolean overrideOntology, String sourceLang, String destLang,
			String destOntologyPath) {
		super();
		this.ontology = ontology;
		this.overrideOntology = overrideOntology;
		this.sourceLang = sourceLang;
		this.destLang = destLang;
		this.destOntologyPath = destOntologyPath;
		this.textTranslator = new TextTranslator();
	}
	
	public OntologyTranslator(String ontologyPath, boolean overrideOntology, String sourceLang, String destLang,
			String destOntologyPath) throws OWLOntologyCreationException, FileNotFoundException {
		this(Utils.loadOntologyFromPath(ontologyPath), overrideOntology, sourceLang, destLang, destOntologyPath);
		this.origOntologyPath = ontologyPath;
	}

	/**
	 * Performs the translation of the ontology
	 * @throws OWLOntologyStorageException
	 * @throws FileNotFoundException
	 */
	public void translate() throws OWLOntologyStorageException, FileNotFoundException {
		this.translate(ontology.getSignature());
		if(overrideOntology){
			Utils.saveOntology(ontology, origOntologyPath);
		} else {
			Utils.saveOntology(ontology, destOntologyPath);
		}
	}

	private void translate(Collection<OWLEntity> owlEntities) {
		OWLAnnotationProperty labelAnnotationProperty = ontology.getOWLOntologyManager().getOWLDataFactory()
				.getRDFSLabel();
		for (OWLEntity owlEntity : owlEntities) {
			/* If this entity already has the annotation in the destination language, dont do anything */
			if(this.alreadyHasDestLang(owlEntity, labelAnnotationProperty)){
				continue;
			}
			for (OWLAnnotation owlAnnotation : EntitySearcher.getAnnotationObjects(owlEntity, ontology,
					labelAnnotationProperty)) {
				if (owlAnnotation.getValue() instanceof OWLLiteral) {
					OWLLiteral literal = (OWLLiteral) owlAnnotation.getValue();
					if (literal.hasLang(sourceLang)) {
						String label = literal.getLiteral();
						try {
							String translatedLabel = textTranslator.translate(sourceLang, destLang, label);
							Utils.addLabel(ontology, owlEntity, translatedLabel, destLang);
							LOGGER.info("Translated\t" + label + "\t=>\t" + translatedLabel);
						} catch (ClientProtocolException e) {
							LOGGER.log(Level.SEVERE, "Error translating the ontology", e);
						} catch (IOException e) {
							LOGGER.log(Level.SEVERE, "Error translating the ontology", e);
						}
					}
				}
			}
		}
	}

	/**
	 * Tells if the entity already has the destination language in the specified annotation property.
	 * @param owlEntity
	 * @param owlAnnotationProperty
	 * @return
	 */
	private boolean alreadyHasDestLang(OWLEntity owlEntity, OWLAnnotationProperty owlAnnotationProperty) {
		for (OWLAnnotation owlAnnotation : EntitySearcher.getAnnotationObjects(owlEntity, ontology,
				owlAnnotationProperty)) {
			if (owlAnnotation.getValue() instanceof OWLLiteral) {
				OWLLiteral literal = (OWLLiteral) owlAnnotation.getValue();
				if (literal.hasLang(destLang)) {
					return true;
				}
			}
		}
		return false;
	}



}
