package es.um.es.semanticweb.tests;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.junit.Before;
import org.junit.Test;

import es.um.es.semanticweb.translator.javaObjects.TextTranslator;

public class TranslatorTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void translatorTest() throws ClientProtocolException, IOException {
		TextTranslator translator = new TextTranslator();
		String translatedWord = translator.translate(null, "es", "car");
		assertEquals("coche", translatedWord);
	}

}
