Command line utility that translates the labels of an ontology.

Usage: java -jar ontology-label-translator.jar -f inputOntologyPath -o outputOntologyPath -l originalLang -L destinationLang

Example of the translation of english labels to spanish in dbpedia ontology owl file:
java -jar ontology-label-translator.jar -f dbpedia_2016-04.owl -o dbpedia_2016-04_translated.owl -l en -L es

The program iterates over the entities of the ontology. For each entity, it obtains the labels in the original language and then they are translated into the destination language unless the entity already has a label in this language.

Finally, if -o option is specified, the new ontology with the translations are stored in outputOntologyPath. If not, the translated ontology will override the original ontology.